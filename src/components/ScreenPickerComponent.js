import React, { Component } from "react";
import { connect } from 'react-redux';
import ProductList from "./shoppingCart/productList";
import { GetProductListAction } from "../redux/actions/GetProductListAction";
import { GetAllProductsListAction } from "../redux/actions/GetAllProductsListAction";
import SetupProducts from "./setupProducts/SetupProducts";
import { saveFormDataAction } from "../redux/actions/saveFormDataAction";
import { DeleteProductsAction } from "../redux/actions/DeleteProductsAction";

class ScreenPickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: this.props.screen,
      
    };
    this.ShowView = this.ShowView.bind(this);
  }

  componentDidMount() {
    this.props.GetProductListAction();
    this.props.GetAllProductsListAction();
  }

  ShowView = view => {
    switch (view) {
      /* case "setupNewProduct":
        return <SetupNewProductMainComponent
          productList={this.state.data}
        // productList={this.props.productList && this.props.productList.productList ? this.props.productList.productList : []} 
        /> */
      case "setupNewProduct":
        return <SetupProducts
          //  productList={this.state.data}
          saveFormData={this.props.saveFormData}
          DeleteProductsAction={this.props.DeleteProductsAction}
          GetProductListAction={this.props.GetProductListAction}
          productList={this.props.productList && this.props.productList.productList ? this.props.productList.productList : []}
        />
      case "shoppingPage":
        return <ProductList
          // products={this.state.data}
          products={this.props.fetchProductList && this.props.fetchProductList.fetchProductList ? [...this.props.fetchProductList.fetchProductList] : []}
        />
      default:
        return "PAGE NOT FOUND(Please refer README.md file in Java project for instructions)"
    }
  };


  render() {
    let view = this.state.screen;
    return <div className="main-container">{this.ShowView(view)}
    </div>;
  }
}

const mapDipatchToProps = dispatch => {
  return {
    GetProductListAction: () => {
      dispatch(GetProductListAction());
    },
    GetAllProductsListAction: () => {
      dispatch(GetAllProductsListAction());
    },
    saveFormData: (array) => {
      dispatch(saveFormDataAction(array));
    },
    DeleteProductsAction: (array) => {
      dispatch(DeleteProductsAction(array));
    }
  }
}

const mapStateToProps = state => {
  return {
    fetchProductList: state.fetchProductList,
    productList: state.productList,
    glabaleData: state.glabaleData,
    savedSetupProducts: state.savedSetupProducts
  }
}

export default connect(mapStateToProps, mapDipatchToProps)(ScreenPickerComponent);
