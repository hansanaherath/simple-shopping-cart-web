import React, { Component } from 'react';
import { Form, FormGroup, Col, Button, Row, Card } from "react-bootstrap";
import PropTypes from 'prop-types';
import SingleProductPopUp from './SingleProductPopUp';
import ShoppingCartImageComponent from './ShoppingCartImageComponent';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalShow: false
        }
    }

    setModalShow = () => {
        this.setState({
            modalShow: true
        })
    }

    setModalHide = () => {
        this.setState({
            modalShow: false
        })
    }

    render() {
        const { productName, singleUnitPrice, curency, image } = this.props;

        return (
            <div className="product_thumb">
                <div className="caption">
                    <Card style={{ width: '19rem' }}>
                        <Card.Body>
                            <ShoppingCartImageComponent
                                image={image} />
                            <div className="product-details">
                                <h3>{productName}</h3>
                                <h2 className="product__price">{singleUnitPrice} {curency}</h2><br />
                                <Button className="block" onClick={this.setModalShow} >Add to cart</Button>

                            </div>
                        </Card.Body>
                    </Card>
                    <SingleProductPopUp
                        show={this.state.modalShow}
                        setModalHide={this.setModalHide}
                        addToCart={this.props.addToCart}
                        clearCart={this.props.clearCart}
                        {...this.props}
                    />
                </div>
            </div>
        );
    }
}

export default Product;