import React, { Component } from 'react';
import Product from './product';
import { Row, Col, Container } from 'react-bootstrap';
import CheckOutCartMain from './CheckOutCartMain';

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      cartArray: [],
      totalItem: 0,
      subTotal: 0,
      delivaryCharge: 0,
      discount: 0,
      grandTotal: 0
    }
  }

  addToCart = (obj) => {
    let { totalItem, subTotal, delivaryCharge, discount, grandTotal } = this.state
    totalItem += parseInt(obj.itemCount)
    subTotal += obj.totalPrice
    discount += obj.discount
    this.setState({
      totalItem: totalItem,
      subTotal: subTotal,
      discount: discount,
      grandTotal : grandTotal
    })
  }

  clearCart = () => {
    this.setState({
      totalItem: 0,
      subTotal: 0,
      discount: 0
    })
  }

  render() {
    const [...products] = this.props.products
    return (
      <div>
        <Container fluid>
          <Row>
            <Col lg={9}>
              <h2>Products</h2>
              <ul>
                {products.map(product => (
                  <li key={product.id}>
                    <Product {...product}
                      addToCart={this.addToCart}
                      clearCart={this.props.clearCart}
                      image={"https://images.pexels.com/photos/1351238/pexels-photo-1351238.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"} />
                  </li>
                ))}
              </ul>
            </Col>
            <Col lg={3}>
              <div className="cart-box">
                <CheckOutCartMain
                  {...this.state}
                  clearCart={this.clearCart} />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}


export default ProductList;