import React, { Component } from "react";
import { Row, Col, Button, Card } from "react-bootstrap";

class CheckOutCartMain extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    clearCart = ()=>{
        this.props.clearCart()
    }

    render() {
        let { totalItem, subTotal, delivaryCharge, discount, grandTotal } = this.props

        return (
            <div>
                <h2>Order Summery</h2>
                <Card className="p-4">
                <Row>
                    <Col>
                        <p className="lead-3">Item Count</p>
                    </Col>
                    <Col>
                        <p className="lead-3 float-right">{totalItem}</p>
                    </Col>
                </Row>

                <Row>
                    <Col>
                    <p className="lead-3">Sub Total </p>
                    </Col>
                    <Col><p className="lead-3 float-right">{subTotal.toFixed(3)}</p></Col>

                </Row>
  
                <Row>
                    <Col> <p className="lead-3">Delivary Carge</p></Col>
                    <Col><p className="lead-3 float-right">{delivaryCharge.toFixed(3)}</p></Col>

                </Row>
                <Row>
                    <Col><p className="lead-3">Discount</p></Col>
                    <Col> <p className="lead-3 float-right">{discount.toFixed(3)}</p></Col>

                </Row>
                <hr className="p-0"/>
                <Row>
                    <Col><p className="lead"><b>Total</b></p></Col>
                    <Col><p className="lead float-right"><b>{grandTotal.toFixed(3)}</b></p></Col>

                </Row>
                <hr className="p-0"/>
                <Row>
                    <Col>
                        <Button className="custom-btn">Checkout</Button>
                    </Col>
                </Row>
                <Row><Col>
                    <Button className="custom-btn mt-2" variant="outline-warning" onClick={(e) =>this.clearCart(e)}>Clear cart</Button>
                    </Col></Row>
                </Card>

            </div>
        )
    }
}

export default CheckOutCartMain;