import React, { Component } from "react";
import { Col, Button } from "react-bootstrap";
import { toast } from 'react-toastify';

class AddButtonComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    notifyError = (message) => {
        toast.error(message, {
            position: "top-right",
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: false
        });
    }

    addDataToGrid = (gridArray, form, editObj) => {
        let arrayKey = ["standardInfo", "productPrice", "productDiscounts"]
        let globalValidation = true
        let savingObj = { "id": null, "singleUnitPrice": null, "curency": "USD" }
        arrayKey.forEach((keyName, j) => {
            form.forEach((ele, i) => {
                if (ele.hasOwnProperty(keyName)) {
                    let isValidate = this.dataValidation(ele[keyName])
                    if (isValidate == false) {
                        globalValidation = false
                    } else if (isValidate) {
                        let array = ele[keyName]
                        array.forEach(element => {
                            savingObj[element.id] = element.value
                        });
                    }
                }
            })
        })

        if (globalValidation) {
            if (editObj.hasOwnProperty("index") && editObj.index !== null) {
                savingObj["id"] = editObj.id
                savingObj["singleUnitPrice"] = editObj.singleUnitPrice
                savingObj["curency"] = editObj.curency
                gridArray[editObj.index] = savingObj
            } else {
                gridArray.push(savingObj)
            }
            this.props.resetForm(form)
        }else{
            this.notifyError("Fields Can Not Be Empty.")
            alert("Fields Can Not Be Empty.")
        }
    }


    dataValidation = (array) => {
        let isGlobalValidation = true
        array.forEach(element => {
            let isValidated = true;
            element.validation.forEach(validation => {
                if (validation === "isEmpty") {
                    if (element.value == null || element.value == "") {
                        isValidated = false
                    }
                }
            })

            if (isValidated == false) {
                isGlobalValidation = false
            }
        });
        return isGlobalValidation;
    }

    render() {
        const { gridArray, form, editObj } = this.props
        return (
            <Col md="4" >
                <div className="section-body">
                    {editObj.hasOwnProperty("index") && editObj.index !== null ?
                        <Button className="controller-btn btn btn-primary custom-btn" onClick={() => { this.addDataToGrid(gridArray, form, editObj) }}><i className="fas fa-save"></i>Done</Button> :
                        <Button className="controller-btn btn btn-primary custom-btn" onClick={() => { this.addDataToGrid(gridArray, form, editObj) }}><i className="fas fa-save"></i>Add Product</Button>
                    }
                </div>
            </Col>

        )
    }
}

export default AddButtonComponent;