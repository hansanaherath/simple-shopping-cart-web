import React, { Component } from "react";
import { Row, Col, Button } from "react-bootstrap";
import SetupNewProduct from "./SetupNewProduct";
import AddButtonComponent from "./AddButtonComponent";

class LoadSetupComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: null
        }
    }

    resetForm = (form) => {
        let arrayKey = ["standardInfo", "productPrice", "productDiscounts"]
        arrayKey.forEach(element => {
            form.forEach(ele => {
                if (ele.hasOwnProperty(element)) {
                    ele[element].forEach(obj => {
                        obj.value = ""
                    });
                }
            });
        });

        this.props.resetForm(form)
    }

    //doneButton = index => { this.setState({ index: index }) }

    render() {
        //const {index} =this.state
        const { form, gridArray, editObj } = this.props
        let componentArray = [form[0].standardInfo, form[1].productPrice, form[2].productDiscounts]
        const array = [{ "key": "standardInfo", "value": "Standard Info" }, { "key": "productPrice", "value": "Price Info" }, { "key": "productDiscounts", "value": "Discount Info" }]
        let product = array.map((item, i) => {
            return (
                <Col md="4" key={i}>
                    <h5 className="section-body">{item.value}</h5>
                    <SetupNewProduct
                        data={componentArray[i]}
                        name={{ "key": item.key }}
                        addDataToRedux={this.props.updateState}
                    //doneButton={this.doneButton}
                    />
                </Col>
            )
        })
        return (
            <div>
                <div className="section-title">Product Info</div>
                <Row>{product} </Row>

                <Row className="mb-5 mt-3">
                    <Col md="8"></Col>
                    <AddButtonComponent
                        resetForm={this.resetForm}
                        form={form}
                        gridArray={gridArray}
                        editObj={editObj}
                    />
                </Row>
                <hr />
            </div>
        )
    }
}

export default LoadSetupComponent;