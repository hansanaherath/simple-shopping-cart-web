import React, { Component } from "react";
import TableHeadComponent from "../commonComponents/TableHeadComponent";
import TableBodyComponent from "../commonComponents/TableBodyComponent";
import { Table } from "react-bootstrap";

class LoadTableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ["Code", "Name", "Description", "Currency", "Number Of Cartons", "Units Per Cartons", "Single Carton Price", "Profit Margine For Single Corton(%)", "Minimum Number Of Cartons", "Discount(%)", "Action"]
        }
    }

    remove = (e, gridArray, deleteArray) => {
        if (e.target.id.split("@@@")[0] === null) {
            gridArray.splice(e.target.id.split("@@@")[1], 1);
        } else {
            deleteArray.push(gridArray[e.target.id.split("@@@")[1]])
            gridArray.splice(e.target.id.split("@@@")[1],1)
        }
        this.props.remove(gridArray, deleteArray)
    }

    edit = (e, gridArray, form) => {
        let index = e.target.id.split("@@@")[1]
        let id = e.target.id.split("@@@")[0]
        let editObj = gridArray[index]
        let keySet = ["standardInfo", "productPrice", "productDiscounts"]
        form.forEach(mainObj => {
            keySet.forEach(key => {
                if (mainObj.hasOwnProperty(key)) {
                    let dataArray = mainObj[key]
                    dataArray.forEach(element => {
                        element.value = editObj[element.id]
                    });
                }
            });
        });
        editObj["index"] = index
        this.props.edit(form, editObj)
    }

    render() {
        const { tableHead } = this.state;
        const { gridArray, deleteArray, form } = this.props
        return (
            <div className="data-table-responsive">
                <Table striped bordered hover className="assign-combination-table mt-5">
                    <thead>
                        <TableHeadComponent
                            dataArray={tableHead}
                        />
                    </thead>
                    <TableBodyComponent
                        dataArray={gridArray}
                        remove={(e) => this.remove(e, gridArray, deleteArray)}
                        edit={(e) => this.edit(e, gridArray, form)}
                    />
                </Table>
            </div>
        )
    }
}

export default LoadTableComponent;