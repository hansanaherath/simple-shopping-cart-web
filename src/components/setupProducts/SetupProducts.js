import React, { Component } from "react";
import LoadTableComponent from "./LoadTableComponent";
import LoadSetupComponent from "./LoadSetupComponent";
import { Container } from "react-bootstrap";
import SaveButtonComponent from "./SaveButtonComponent";

class SetupProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: [
                {
                    "standardInfo": [
                        { "label": "Product Code", "type": "text", "id": "productCode", "placeholder": "", "value": "", "validation": ["isEmpty"] },
                        { "label": "Product Name", "type": "text", "id": "productName", "placeholder": "", "value": "", "validation": ["isEmpty"] },
                        { "label": "Product Description", "type": "text", "id": "productDescription", "placeholder": "", "value": "", "validation": ["isEmpty"] },
                    ]
                },
                {
                    "productPrice": [
                        { "label": "Number Of Available Cartons", "type": "number", "id": "numOfCartons", "placeholder": "", "value": "", "validation": ["isEmpty"] },
                        { "label": "Units Per Cartons", "type": "number", "id": "unitsPerCarton", "placeholder": "", "value": "", "validation": ["isEmpty"] },
                        { "label": "Single Carton Price", "type": "number", "id": "singleCartonPrice", "placeholder": "", "value": "", "validation": ["isEmpty"] },
                        { "label": "Profit Margine For Single Corton", "type": "number", "id": "margineForSingleUnit", "placeholder": "", "value": "", "validation": ["isEmpty"] }
                    ]
                },
                {
                    "productDiscounts": [
                        { "label": "Number Of Cartons Should Buy", "type": "number", "id": "minNumOfCartons", "placeholder": "", "value": "", "validation": ["isEmpty"] },
                        { "label": "Discount For Carton (%)", "type": "number", "id": "discount", "placeholder": "", "value": "", "validation": ["isEmpty"] }
                    ]
                }
            ],
            gridArray: [],
            deleteArray: [],
            editObj: {}
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.productList && props.productList.length > 0 && props.productList != state.gridArray) {
            return {
                gridArray: props.productList
            }
        }
        return null
    }

    updateState = (obj, key) => { this.setState({ [key]: obj[key] }) }

    resetForm = (form) => { this.setState({ form: form, editObj: {} }) }

    remove = (gridArray, deleteArray) => {
        this.setState({
            gridArray: gridArray,
            deleteArray: deleteArray
        })
    }

    edit = (form, editObj) => {
        this.setState({ form: form, editObj: editObj })
    }

    save = () => {
        if (this.state.gridArray.length > 0) {
            this.props.saveFormData(this.state.gridArray);
            this.setState({gridArray : []},()=>{this.props.GetProductListAction()})
        }
        if (this.state.deleteArray.length > 0) {
            this.props.DeleteProductsAction(this.state.deleteArray);
        }
    }

    render() {
        const productList = [...this.props.productList]
        const { form, gridArray, deleteArray, editObj } = this.state;
        return (
            <div className="product-details-container section">
                <Container fluid>
                    <LoadSetupComponent
                        form={form}
                        updateState={this.updateState}
                        resetForm={this.resetForm}
                        gridArray={gridArray}
                        editObj={editObj}
                    />
                    <LoadTableComponent
                        gridArray={gridArray}
                        deleteArray={deleteArray}
                        remove={this.remove}
                        form={form}
                        edit={this.edit} />

                    <SaveButtonComponent
                        save={this.save} />
                    <hr />
                </Container>
            </div>
        )
    }
}

export default SetupProducts;