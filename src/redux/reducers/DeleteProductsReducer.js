const DeleteProductsReducer = (state = {}, action) => {
    switch (action.type) {

        case 'FETCHING_DELETE_SETUP_PRODUCTS':
            return Object.assign({}, state, {
                fetching: true,
                productDeleted: {}
            });

        case 'DELETE_SETUP_PRODUCTS_SUCCESS':
            return Object.assign({}, state, {
                fetching: false,
                productDeleted: action.payload.data

            });

        case 'DELETE_SETUP_PRODUCTS_FAILED':
            return Object.assign({}, state, {
                fetching: false,
                productDeleted: {}
            });
        case 'DELETE_SETUP_PRODUCTS_RESET':
            return Object.assign({}, state, {
                fetching: false,
                productDeleted: {}
            });
        default:
            return state;
    }

}

export default DeleteProductsReducer;