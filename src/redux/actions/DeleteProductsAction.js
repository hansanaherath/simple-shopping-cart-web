import axios from 'axios';

export const DeleteProductsAction = (array) => {

    return (dispatch, getState) => {
        dispatch({
            type: 'FETCHING_DELETE_SETUP_PRODUCTS', 
            payload: { 
                fetching: true, 
                data: {} 
            } 
        });

        axios({
            method: 'DELETE',
            url: "/setups/products/deleteProductList",
            headers:{
              'Access-Control-Allow-Origin':'*',
              'Content-Type': 'application/json;charset=UTF-8',
          },
          data : array
           /*  data: array,
             'headers': {
              'Content-Type': 'application/json',
              'Cache-Control': 'max-age=0, private, must-revalidate',
              'Pragma' : 'no-cache',
              'X-Requested-With': 'XMLHttpRequest'
            } */,
          }).then(response => {
            dispatch({ 
                type: 'DELETE_SETUP_PRODUCTS_SUCCESS', 
                payload: { 
                    fetchingData: false, 
                    data: response.data 
                } 
            });
        }).catch(e => {
            dispatch({ 
                type: 'DELETE_SETUP_PRODUCTS_FAILED', 
                payload: { 
                    fetchingData: false, 
                    data: e 
                } 
            });
        });
    }
}

export const resetDeleteProductsAction = () => {
    return (dispatch) => {
      dispatch({
        type: 'DELETE_SETUP_PRODUCTS_RESET',
        payload: {
          fetching: false,
          data: {}
        }
      });
    }
  }